<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware' => 'cors'], function ($api) {

    $api->post('register', 'App\Http\Controllers\API\AuthController@register');

    $api->post('login', 'App\Http\Controllers\API\AuthController@login');

    $api->group(['middleware' => 'api.auth'], function ($api) {

        $api->post('register/email', 'App\Http\Controllers\API\AuthController@registerEmail');

        $api->post('register/phone', 'App\Http\Controllers\API\AuthController@registerPhone');

        $api->post('register/profile', 'App\Http\Controllers\API\ProfileController@registerProfile');

        $api->post('profile/photo', 'App\Http\Controllers\API\ProfileController@uploadPhoto');

        $api->post('phone/code', 'App\Http\Controllers\API\AuthController@getPhoneCode');

        $api->post('logout', 'App\Http\Controllers\API\AuthController@logout');

//        $api->get('company', 'App\Http\Controllers\API\CompanyController@index');
//
//        $api->get('company/{id}', 'App\Http\Controllers\API\CompanyController@show')->where('id', '[0-9]+');
//
//        $api->put('company/{id}', 'App\Http\Controllers\API\CompanyController@update')->where('id', '[0-9]+');
//
//        $api->post('company/delete/{id}', 'App\Http\Controllers\API\CompanyController@delete')->where('id', '[0-9]+');

        $api->post('company', 'App\Http\Controllers\API\CompanyController@store');

//        $api->get('contact', 'App\Http\Controllers\API\ContactController@index');

        $api->post('contact', 'App\Http\Controllers\API\ContactController@store');

        $api->get('contact/me', 'App\Http\Controllers\API\ContactController@getMyCards');

        $api->get('contact/list', 'App\Http\Controllers\API\ContactController@getContacts');

        $api->post('contact/share', 'App\Http\Controllers\API\ContactController@addgit ');

//        $api->get('contact/personal', 'App\Http\Controllers\API\ContactController@getPersonalContacts');
//
//        $api->get('contact/business', 'App\Http\Controllers\API\ContactController@getBusinessContacts');

        $api->post('contact/share', 'App\Http\Controllers\API\ContactController@share');

        $api->post('contact/add', 'App\Http\Controllers\API\ContactController@addCard');

        $api->post('contact/{id}', 'App\Http\Controllers\API\ContactController@update')->where('id', '[0-9]+');

        $api->post('contact/delete/{id}', 'App\Http\Controllers\API\ContactController@destroy')->where('id', '[0-9]+');

        $api->post('profile', 'App\Http\Controllers\API\ProfileController@update');

        $api->get('profile', 'App\Http\Controllers\API\ProfileController@index');

    });
});

