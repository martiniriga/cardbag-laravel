<?php

use Faker\Generator as Faker;
$items = ["personal", "business"];
$company_count = 5;//\App\Company::count();
$factory->define(App\Contact::class, function (Faker $faker) use ($items,$company_count){
    return [
        'phone' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'occupation' => $faker->jobTitle,
        'website' => $faker->url,
        'physical_address' => $faker->address,
        'postal_address' => $faker->postcode,
        'contact_type' => $items[array_rand($items)],
        'company_id' => rand(1, 5),
    ];
});
