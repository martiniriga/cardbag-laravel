<?php

use App\Contact;
use App\User;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::all();
        //create personal cards
        for ($i = 0; $i < $user->count(); $i++ ){
            $contact = factory('App\Contact')->create(['contact_type' => 'personal']);
            $contact->user()->attach($user[$i]->id,['is_owner' =>1]);
        }
        //create business cards
        for ($i = 0; $i < $user->count(); $i++ ){
            $contact = factory('App\Contact')->create(['contact_type' => 'business']);
            $contact->user()->attach($user[$i]->id,['is_owner' =>1]);
        }
        $contacts = Contact::all();
        $j =5;
        //share all the cards
        for ($i = 0; $i < $contacts->count(); $i++ ){
            do {
                $recipient_id = random_int(1,5); //Only 5 users
            } while ($recipient_id == $i);
            $contacts[$i]->user()->attach($j,['recipient_id' =>$recipient_id]);
            $j--;
        }
    }
}
