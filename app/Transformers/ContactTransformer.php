<?php

namespace App\Transformers;
use App\Contact;
use Illuminate\Support\Facades\Storage;
use League\Fractal\TransformerAbstract;


class ContactTransformer extends TransformerAbstract
{
    public function transform(Contact $contact)
    {
        $photo = $contact->user()->first()->photo_url;

        if($photo){
            $photo= asset("storage/".$photo);
        }

        $company = $contact->company()->first();

        if($company){
            $company = $company->name;
        }else{
            $company = null;
        }

        return [
            'id' => (int) $contact->id,
            'names' => $contact->user()->first()->name,
            'company' => $company,
            'email' => $contact->email,
            'phone' => $contact->phone,
            'photo_url' => $photo,
            'occupation' => ucfirst($contact->occupation),
            'physical_address' => $contact->physical_address,
            'postal_address' => $contact->postal_address,
            'website' => $contact->website,
            'type' => ucfirst($contact->contact_type)
        ];
    }
}