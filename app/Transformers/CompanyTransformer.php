<?php

namespace App\Transformers;

use App\Company;
use League\Fractal\TransformerAbstract;


class CompanyTransformer extends TransformerAbstract
{
    public function transform(Company $company)
    {
        return [
            'id'        => (int) $company->id,
            'name'      => ucfirst($company->name)
        ];
    }
}