<?php


namespace App\Transformers;
use App\User;
use League\Fractal\TransformerAbstract;


class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $photo = $user->photo_url;

        if($photo){
            $photo= asset("storage/".$photo);
        }

        $verified_email = $user->email_verified_at;
        $verified_phone = $user->phone_verified_at;
        if($verified_email){
            $verified_email = true;
        }else{
            $verified_email = false;
        }

        if($verified_phone){
            $verified_phone = true;
        }else{
            $verified_phone = false;
        }

        return [
            'id'        => (int) $user->id,
            'name' => ucfirst($user->name),
            'email' => $user->email,
            'photo_url' => $photo,
            'phone' => $user->phone,
            'verified_email' => $verified_email,
            'verified_phone' => $verified_phone
        ];
    }
}