<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Support\Facades\Response;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Schema::defaultStringLength(191);

         app('Dingo\Api\Exception\Handler')->register(function (UnauthorizedHttpException $exception) {
            return Response::make([
                'status'=>'error',
                'error' => [
                    'code' => 'input_invalid',
                    'message' => $exception->getMessage()
                ]
            ], 401);
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
