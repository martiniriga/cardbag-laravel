<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required',
            'email' => 'email|max:255|nullable',
            'address' => 'string|max:255|nullable',
            'website' => 'string|max:255|nullable',
            'contact_type' => 'required|in:personal,business',
            'company' => 'string|max:255|nullable'
        ];

    }
}
