<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|String|max:191',
            'photo' => 'image|mimes:jpeg,jpg,bmp,png'
//            'phone' => 'required|max:40',
//            'email' => 'email|max:191',
//            'website' => 'url|max:191',
//            'address' => 'max:191'
        ];
    }
}
