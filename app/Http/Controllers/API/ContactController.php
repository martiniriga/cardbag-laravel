<?php

namespace App\Http\Controllers\API;

use App\Company;
use App\Contact;
use App\Share;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;
use App\Transformers\ContactTransformer;
use Illuminate\Support\Facades\DB;


class ContactController extends BaseController
{

    public function index()
    {
        $contacts = Contact::join('contact_user','contacts.id','=','contact_user.contact_id')
            ->join('users','contact_user.user_id','=','users.id')
            ->orderBy('users.name','asc')
            ->get();
        return $this->collection($contacts, new ContactTransformer);
    }

    public function getMyCards()
    {
        $user_id = auth()->user()->id;

        $contacts = Contact::select('id','email','phone','occupation','physical_address','postal_address','website','company_id','contact_type')
            ->whereHas('user', function ($query) use ($user_id) {
                        $query->where('user_id', $user_id)
                            ->where('is_owner',1);
                    })
                    ->with(['user'=> function ($query) use ($user_id) {
                                $query->where('user_id', $user_id)
                                      ->where('is_owner',1);
                        },
                        'company'=> function ($query) {
                            $query->select('id','name');
                        },
                    ])->get();

        return $this->collection($contacts, new ContactTransformer);
    }

    public function getContacts(Request $request)
    {
        $searchValue = $request->input('query'); 

        $user_id = auth()->user()->id;
        $query = Contact::join('contact_user','contacts.id','=','contact_user.contact_id')
            ->leftjoin('companies','contacts.company_id','=','companies.id')
            ->where('contact_user.is_owner',0)
            ->where('contact_user.recipient_id',$user_id )
            ->join('users','contact_user.user_id','=','users.id')     
            ->orderBy('users.name','asc');
            
             
        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('users.name', 'like', '%' . $searchValue . '%');
            });
        }

        $contacts = $query->get();

        return $this->collection($contacts, new ContactTransformer);
    }


    public function getBusinessContacts()
    {
        $user_id = auth()->user()->id;
        $contacts = Contact::join('contact_user','contacts.id','=','contact_user.contact_id')
            ->where('contact_type','business')
            ->leftjoin('companies','contacts.company_id','=','companies.id')
            ->join('users','contact_user.user_id','=','users.id')
            ->where('contact_user.is_owner',0)
            ->where('contact_user.recipient_id',$user_id )
            ->orderBy('users.name','asc')
            ->get();

        return $this->collection($contacts, new ContactTransformer);
    }


    public function getPersonalContacts()
    {
        $user_id = auth()->user()->id;
        $contacts = Contact::join('contact_user','contacts.id','=','contact_user.contact_id')
            ->where('contact_type','personal')
            ->leftjoin('companies','contacts.company_id','=','companies.id')
            ->join('users','contact_user.user_id','=','users.id')
            ->where('contact_user.is_owner',0)
            ->where('contact_user.recipient_id',$user_id )
            ->orderBy('users.name','asc')
            ->get();

        return $this->collection($contacts, new ContactTransformer);
    }

    public function store(ContactRequest $request)
    {
        $contact = Contact::Create([
            'occupation' => $request->input('occupation'),
            'phone' => $request->input('phone'),
            'email'=> $request->input('email'),
            'website'=> $request->input('website'),
            'physical_address'=> $request->input('physical_address'),
            'postal_address'=> $request->input('postal_address'),
            'contact_type' => $request->input('contact_type')
        ]);

        $company = $request->input('company');
        if($company != null){
            $co = Company::where('name',ucwords($company))->first();
            if($co){
                $contact->company_id = $co->id;
            }else{
                $co = Company::create(['name' =>ucwords($company)]);
                $contact->company_id = $co->id;
            }
            $contact->save();
        }
        $type = $request->input('contact_type');
        $user_id = auth()->user()->id;

        if ($contact) {
            $contact->user()->attach($user_id,['is_owner' => 1]);
            return $this->response->created();
        }

        return $this->response->errorBadRequest();
    }

    public function show($id)
    {
        $contact = Contact::select('id','email','phone','occupation','physical_address','postal_address','website','company_id','contact_type')
            ->find($id)
            ->with(['user', 'company'=> function ($query) {
                    $query->select('id','name');
                },
            ])->first();

        if($contact){
            return $this->item($contact,new ContactTransformer());
        }
        return $this->response->errorNotFound();
    }

    public function update(ContactRequest $request, $id)
    {
        $contact = Contact::find($id);
        if($contact){
            $contact->email = $request->input('email');
            $contact->phone = $request->input('phone');
            $contact->occupation = $request->input('occupation');
            $contact->physical_address = $request->input('physical_address');
            $contact->postal_address = $request->input('postal_address');
            $contact->website = $request->input('website');
            $company = $request->input('company');
            if($company != null){
                $co = Company::where('name',ucwords($company))->first();
                if($co){
                    $contact->company_id = $co->id;
                }else {
                    $co = Company::create(['name' => ucwords($company)]);
                    $contact->company_id = $co->id;
                }
            }
            $contact->save();
            return $this->item($contact,new ContactTransformer());
        }
        return $this->response->errorBadRequest();
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);

        if ($contact) {

            $contact_owner = $contact->user()->wherePivot('is_owner',1)->first();

            if($contact_owner->id == auth()->user()->id){
                //  if user allow delete
                $contact->delete();

                return response()->json(["message" => "Contact removed successfully"]);

            }else{
                $user_id = auth()->user()->id;

            //    if linked to user unlink
                $linked_contact = $contact->user()->where('user_id',$user_id)->first();

                if($linked_contact){

                    $contact->user()->detach($user_id);

                    return response()->json(["message" => "Contact removed successfully"]);

                }else{

                    return $this->response->errorBadRequest();

                }
            }

        }
        return $this->response->errorBadRequest();
    }

    public function addCard(Request $request)
    {
        $this->validate($request,[
            'token' => 'required|exists:shares,token',
        ]);

        $share = Share::where('token',$request->input('token'))->first();

        $contact = Contact::find($share->contact_id);

        $user = auth()->user();

        $contact_owner = $contact->user()->wherePivot('is_owner',1)->first();

        //you cant add your own contact
        if($contact_owner->id != $user->id){
            $contact->user()->attach($contact_owner->id,['recipient_id' =>$user->id]);
        }

        $share->delete();

        return response()->json(["message" => "Contact added successfully"]);
    }

    public function share(Request $request)
    {
        $this->validate($request,[
            'contact_id' => 'required|exists:contacts,id'
        ]);

        $user = auth()->user();

        $contact_id = $request->input('contact_id');
        $contact = Contact::find($contact_id);
         //   verify owner id of sharer
        $contact_owner = $contact->user()->wherePivot('is_owner',1)->first();
        if($contact_owner->id == $user->id){

            do {
                $token = getToken();
            } while (Share::where('token', $token)->count() > 0);

            $share = Share::where('contact_id' , $contact_id)->first();

            if($share){

                $share->token = $token;
                $share->save();

            }else{

                Share::create([
                    'contact_id' => $contact_id,
                    'token' => $token
                ]);
            }


            return response()->json([
                'token' => $token
            ]);
        }else{
            return response()->json([
                'message' => 'You are not authorised to share this contact'
            ],401);
        }
    }
}
