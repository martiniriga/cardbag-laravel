<?php

namespace App\Http\Controllers\API;

use App\Company;
use App\Http\Requests\CompanyRequest;
use Illuminate\Http\Request;
use App\Transformers\CompanyTransformer;

class CompanyController extends BaseController
{

    public function index()
    {
        $companies= Company::all();
        return $this->collection($companies, new CompanyTransformer);
    }


    public function store(CompanyRequest $request)
    {
        if (Company::Create($request->all())) {
            return $this->response->created();
        }

        return $this->response->errorBadRequest();
    }

    public function show($id)
    {
        $company= Company::find($id);
        if ($company) {
            return $this->item($company, new CompanyTransformer);
        }
        return $this->response->errorNotFound();
    }



    public function update(CompanyRequest $request, $id)
    {
        $company = Company::find($id);
        if ($company) {
            $company->name = $request->input('name');
            $company->save();
            return response()->json(['message'=>'Company updated successfully']);
        }
        return $this->response->errorNotFound();
    }


    public function destroy($id)
    {
        $company = Company::find($id);
        if ($company) {
            $company->delete();
            return $this->response->noContent();
        }
        return $this->response->errorBadRequest();
    }
}
