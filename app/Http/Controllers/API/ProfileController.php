<?php

namespace App\Http\Controllers\API;

use App\Company;
use App\Contact;
use App\Http\Requests\ProfileRequest;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProfileController extends BaseController
{
    public function index()
    {
        $user = auth()->user();
        return $this->Item($user, new UserTransformer);
    }

    public function update(ProfileRequest $request)
    {
        $user = auth()->user();
        $user->name = $request->input('name');
        $user->save();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('profile_pics');
            if($user->photo_url){
                Storage::delete($user->photo_url);
            }
            $user->photo_url= $path;
            $user->save();
        }

        return response()->json(['message'=>'User profile updated successfully']);
    }


    public function registerProfile(Request $request){

        $this->validate($request, [
            'name' => 'required|max:255',
            'company' => 'max:255',
            'postal_address' => 'max:255',
            'physical_address' => 'max:255',
            'website' => 'max:255',
            'occupation' => 'max:255'
        ]);

        $user = auth()->user();

        $user->name = ucwords($request->input('name'));

        $user->save();

        $contact = Contact::Create([
            'occupation' => $request->input('occupation'),
            'phone' => $user->phone,
            'email'=> $user->email,
            'website'=> $request->input('website'),
            'postal_address'=> $request->input('postal_address'),
            'physical_address'=> $request->input('physical_address'),
            'contact_type' => 'personal'
        ]);



        $company = $request->input('company');

        if($company != null){
            $co = Company::where('name',ucwords($company))->first();

            if($co){
                $contact->company_id = $co->id;
            }else{
                $co = Company::create(['name' =>ucwords($company)]);
                $contact->company_id = $co->id;
            }
            $contact->save();
            $contact->user()->attach($user->id,['is_owner' => 1]);
        }


        return response()->json(['message' => 'User Profile created successful']);

    }


    public function uploadPhoto(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image'
        ]);

        $user = auth()->user();

        if($request->hasFile('photo')){
            $path = $request->file('photo')->store('profile_pics');
            if($user->photo_url){
                Storage::delete($user->photo_url);
            }
            $user->photo_url= $path;
            $user->save();

            $photo = $user->photo_url;

            if($photo){
                $photo = asset("storage/".$photo);
            }

            return response()->json([
                'message'=>'Profile picture uploaded successfully',
                'photo_url' => $photo
            ]);
        }

        return response()->json([
            'message'=>'Profile picture upload skipped'
        ],422);

    }

}
