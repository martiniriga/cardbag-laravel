<?php

namespace App\Http\Controllers\API;

use App\Share;
use Illuminate\Http\Request;


class ShareController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function create()
    {

    }

//#TODO stop for now might be overkill
    public function store(Request $request)
    {
        $this->validate($request, [
            'contact_id' => 'required|exists:contacts,id'
        ]);

        $contact_id = $request->input('contact_id');

        $contact = Contact::find($request->input('contact_id'))->wherePivot('is_owner',1);

        if($contact){

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
