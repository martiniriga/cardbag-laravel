<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $fillable = [
        'token', 'contact_id'
    ];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

}
