<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'occupation', 'phone', 'email', 'website', 'physical_address','postal_address','contact_type'
    ];
    public function user()
    {
        return $this->belongsToMany('App\User')->withPivot('user_id','is_owner','recipient_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function share()
    {
        return $this->hasOne('App\Share');
    }

}
