<?php

namespace Tests\Feature;

use App\Company;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAuthenticateUser()
    {
        $user = factory(User::class)->create(['password' => bcrypt('foo')]);
        $this->post('/api/login', ['email' => $user->email, 'password' => 'foo'])
            ->assertJsonStructure(['token']);
    }

    public function testRegisterUser()
    {
        $email = $this->faker->safeEmail;
        $this->post('/api/register', [
            'email' => $email,
            'password' => 'secret',
            'confirm_password' => 'secret',
            'signup' => 'Android'
            ])
            ->assertJsonStructure(['token']);

        $this->assertDatabaseHas('users',[
            'email' => $email
        ]);
    }

//#TODO: Fix buggy test
    public function testValidateUniqueEmailForUser()
    {
        $email = $this->faker->safeEmail;
        factory(User::class)->create(['email' =>$email]);
        $this->post('/api/register', [
            'email' => $email,
            'password' => 'secret',
            'confirm_password' => 'secret',
            'signup' => 'Android'
        ])
//            ->assertJson([
//            'message' => 'The given data was invalid.',
//            ])
            ->assertJsonStructure([
            'message',
            'errors'
            ])
        ->assertStatus(422);

    }


}
