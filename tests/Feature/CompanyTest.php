<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Company;

class CompanyTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetCompanies()
    {
        $user = factory(User::class)->create();
        $this->seed('CompanyTableSeeder');
        $this->get('/api/company',$this->headers($user))
            ->assertJsonCount(5, 'data');
    }
    public function testGetSingleCompany()
    {
        $user = factory(User::class)->create();
        $this->seed('CompanyTableSeeder');
        $this->get('/api/company/1',$this->headers($user))
            ->assertJsonStructure([
                'data' => [ 'id','name' ]
            ]);
    }
    public function testSaveCompany()
    {
        $user = factory(User::class)->create();
        $company = ['name' => 'Webmasters'];
        $this->seed('CompanyTableSeeder');
        $this->post('/api/company',$company,$this->headers($user))
            ->assertStatus(201);
    }

    public function testValidateUniqueCompany()
    {
        $user = factory(User::class)->create();
        $company = ['name' => 'Webmasters'];
        factory(Company::class)->create($company);
        $this->post('/api/company',$company,$this->headers($user))
            ->assertJson(['message'])
            ->assertStatus(422);
    }

}
